#!/bin/bash

set -euf -o pipefail

export PATH=/bin:/sbin

PACKAGES='wget git zile nano net-tools docker-1.13.1 bind-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct openssl-devel httpd-tools NetworkManager python-cryptography python2-pip python-devel  python-passlib java-1.8.0-openjdk-headless ansible "@Development\ Tools"'

update() {
    if ! yum -y update; then
	echo "Could not update to latest centos release!"
	return 1
    fi
}

packages() {
    if ! yum install -y $PACKAGES; then
	echo "Could not install required packages!"
	return 1
    fi
}

hosts_file() {
    if ! grep -q '172.168.0.2' /etc/hosts; then
	echo '172.168.0.2 master' >> /etc/hosts
	echo '172.168.0.3 node01' >> /etc/hosts
	echo '172.168.0.4 node02' >> /etc/hosts
    fi
}

exit_failure() {
    exit 1
}

enable_ssh_access() {
    cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
    return 0
}

if ! update; then
    exit_failure
fi

if ! packages; then
    exit_failure
fi

if ! hosts_file; then
    exit_failure
fi

if ! enable_ssh_access; then
    exit_failure
fi
