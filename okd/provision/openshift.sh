#!/bin/bash

set -euf -o pipefail

PATH=/bin:/sbin
export PATH

OPENSHIFT_RELEASE='3.11'

clone_openshift_ansible() {
    if ! [ -d openshift-ansible ] && git clone https://github.com/openshift/openshift-ansible.git; then
	echo "Could not clone openshift-ansible repository!"
	return 1
    fi

    if ! cd openshift-ansible && git fetch && git checkout release-${OPENSHIFT_RELEASE} && cd ..; then
	echo "Could not check out release"
	return 1
    fi
}

prepare_inventory() {
    if ! sudo chown vagrant:vagrant /etc/ansible/hosts; then
	echo "Could not chown hosts file"
	return 1
    fi

    cat<<EOF >> /etc/ansible/hosts
[masters]
master

[nodes]
master
node01
node02
EOF

}

if ! clone_openshift_ansible; then
    exit 1
fi

if ! prepare_inventory; then
    exit 1
fi
