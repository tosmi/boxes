* Kubernetes setup

- sudo kubeadm init --control-plane-endpoint master.lab
- kubadm token list
- openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'

* Additional Info


You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join master.lab:6443 --token aq4k1h.5s1b2hthefbf4qzb \
    --discovery-token-ca-cert-hash sha256:5709e0f5ea0a98392cf0c47c6a1811ab7bf1f2829fd5cc67edc04d5f3d359ae2 \
    --control-plane

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join master.lab:6443 --token aq4k1h.5s1b2hthefbf4qzb \
    --discovery-token-ca-cert-hash sha256:5709e0f5ea0a98392cf0c47c6a1811ab7bf1f2829fd5cc67edc04d5f3d359ae2
