$node_number = 2

# $node_number = Integer(ENV['NODE_NUMBER']) if ENV.has_key? "NODE_NUMBER"

Vagrant.configure("2") do |config|
  config.ssh.forward_agent = true
  config.vm.box_check_update = false
  config.vm.synced_folder "./", "/vagrant", type: "sshfs"

  config.vm.provider :libvirt do |libvirt|
    libvirt.qemu_use_session = false
  end

  config.vm.provision "file", source: "files/sshkey", destination: "/home/vagrant/.ssh/id_ed25519"
  config.vm.provision "file", source: "files/sshkey.pub", destination: "/home/vagrant/.ssh/id_ed25519.pub"
  config.vm.provision "file", source: "files/bashrc", destination: "/home/vagrant/.bashrc"
  config.vm.provision "common", type: "shell", path: "provision/common.sh"

  config.vm.define "master", primary: true do |config|
    config.vm.box = "centos/7"
    config.vm.hostname = 'master'

    config.vm.network :private_network, :libvirt__network_name => "k8s", :mac => '52:54:00:2d:b0:a1'
    config.vm.network "forwarded_port", guest: 3128, host: 3128
    config.vm.network "forwarded_port", guest: 6443, host: 6443
    config.vm.network "forwarded_port", guest: 80,   host: 8080
    config.vm.network "forwarded_port", guest: 443,  host: 8443

    config.vm.provider :libvirt do |libvirt|
      libvirt.cpus = 2
      libvirt.memory = 2048
    end

    config.vm.provision "kubernetes", type: "shell", path: "provision/k8s.sh"
    config.vm.provision "reboot", type: "shell", path: "provision/reboot.sh"
  end

  (1..$node_number).each do |n|
    config.vm.define vm_name = "node%02d" % n do |config|
      config.vm.box = "centos/7"
      config.vm.hostname = vm_name

      config.vm.network :private_network, :libvirt__network_name => "k8s", :mac => "52:54:00:2d:b0:b%d" % n
      config.vm.provider :libvirt do |libvirt|
        libvirt.cpus = 1
        libvirt.memory = 2048
      end

      config.vm.provision "shell", path: "provision/reboot.sh"
    end
  end
end
