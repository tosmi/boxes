#!/bin/bash

set -euf -o pipefail

export PATH=/bin:/sbin

kubernetes() {
    kubeadm config images pull | tee /var/log/kubeadm_image_pull.log
    kubeadm init \
	    --control-plane-endpoint master.lab \
	    --pod-network-cidr=100.64.0.0/12 \
	    --service-cidr=172.27.0.0/16 \
	| tee /var/log/kubeadm_init.log

    mkdir -p /home/vagrant/.kube
    cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
    chown -R vagrant:vagrant /home/vagrant/.kube/
}

calico() {
    (
	cd /usr/local/bin
	curl -s -O -L  https://github.com/projectcalico/calicoctl/releases/download/v3.10.0/calicoctl
	chmod +x calicoctl
    )

    mkdir -p /etc/calico
    cat <<EOF > /etc/calico/calicoctl.cfg
apiVersion: projectcalico.org/v3
kind: CalicoAPIConfig
metadata:
spec:
  datastoreType: "kubernetes"
  kubeconfig: "/home/vagrant/.kube/config"
EOF

    echo "sleeping 3 minutes to give kubernetes time to start"
    sleep 180
    kubectl apply -f /vagrant/deployments/calico.yaml
}


if ! kubernetes; then
    exit 1
fi

if ! calico; then
    exit 1
fi
