#!/bin/bash

set -euf -o pipefail

export PATH=/bin:/sbin

PACKAGES='dnsmasq skopeo git net-tools docker-1.13.1 bind-utils iptables-services bridge-utils bash-completion kexec-tools psacct openssl-devel httpd-tools NetworkManager python-cryptography python2-pip python-devel python-passlib java-1.8.0-openjdk-headless "@Development\ Tools"'

update() {
    if ! yum -q -y update; then
	echo "Could not update to latest centos release!"
	return 1
    fi
}

packages() {
    if ! yum install -q -y $PACKAGES; then
	echo "Could not install required packages!"
	return 1
    fi
}

kubernetes() {
	cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

	cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

	cat <<EOF > /etc/NetworkManager/conf.d/calico.conf
[keyfile]
unmanaged-devices=interface-name:cali*;interface-name:tunl*
EOF

	sysctl --system

	# Set SELinux in permissive mode (effectively disabling it)
	setenforce 0
	sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

	yum install -q -y kubelet kubeadm kubectl --disableexcludes=kubernetes

	/usr/bin/systemctl enable --now kubelet
	/usr/bin/systemctl enable --now docker

	/usr/sbin/swapoff -a
	/usr/bin/sed -i '/^\/swapfile/d' /etc/fstab
	/usr/bin/rm /swapfile
}

ssh_access() {
    grep -q 'loginkey' /home/vagrant/.ssh/authorized_keys || cat /home/vagrant/.ssh/id_ed25519.pub >> /home/vagrant/.ssh/authorized_keys
}

dnsmasq() {
    # stolen from origin dns config
    def_route=$(/sbin/ip route list match 0.0.0.0/0 | awk '{print $3 }')
    def_route_int=$(/sbin/ip route get to ${def_route} | awk -F 'dev' '{print $2}' | head -n1 | awk '{print $1}')
    def_route_ip=$(/sbin/ip route get to ${def_route}  | awk -F 'src' '{print $2}' | head -n1 | awk '{print $1}')

    cat<<EOF > /etc/NetworkManager/conf.d/dns.conf
[main]
dns=none
EOF
    systemctl restart NetworkManager

    echo "$def_route_ip" > /etc/resolv.conf
    cat<<EOF > /etc/dnsmasq.d/upstream.conf
no-resolv
domain-needed
no-negcache
max-cache-ttl=1
enable-dbus
dns-forward-max=10000
cache-size=10000
bind-dynamic
min-port=1024
interface=eth0
except-interface=lo
server=$def_route
EOF

    cat <<EOF > /etc/dnsmasq.d/libvirt.conf
server=/lab/172.168.0.1
EOF
    cat <<EOF > /etc/dnsmasq.d/kubernetes.conf
server=/cluster.local/172.27.0.10
address=/k8s.lab/172.168.0.2
EOF

    systemctl enable --now dnsmasq.service
}

if ! update; then
    exit 1
fi

if ! packages; then
    exit 1
fi

if ! kubernetes; then
    exit 1
fi

if ! ssh_access; then
    exit 1
fi

if ! dnsmasq; then
    exit 1
fi
