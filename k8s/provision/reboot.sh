#!/bin/bash

set -euf -o pipefail

PATH=/usr/bin:/usr/sbin

INSTALLED_KERNEL=$(rpm -q --qf '%{VERSION}-%{RELEASE}.%{ARCH}\n' kernel-tools)
RUNNING_KERNEL=$(uname -r)

if [ "$INSTALLED_KERNEL" != "$RUNNING_KERNEL" ]; then
    echo ""
    echo "REBOOT REQUIRED!"
    echo ""
fi
