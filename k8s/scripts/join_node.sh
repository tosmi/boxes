#!/bin/bash

set -euf -o pipefail

PATH=/usr/bin:/usr/sbin

SCRIPTNAME=$(basename $0)
NODE=${1:-}

TOKEN=$(kubeadm token list | awk 'NR == 2 {print $1}')
HASH=$(openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //')

usage() {
    echo "${SCRIPTNAME}: ${SCRIPTNAME} <node name>"
    exit 1
}

join() {
    node=$1
    ssh "$node" "[ -f /etc/kubernetes/kubelet.conf ] || sudo kubeadm join master.lab:6443 --token ${TOKEN} --discovery-token-ca-cert-hash sha256:${HASH}"
}

[ -z "$NODE" ] && usage

join "$NODE"
